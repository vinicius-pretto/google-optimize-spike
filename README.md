# Google Optimize Spike

## References

* [Google Optimize](https://developers.google.com/optimize/devguides/experiments)

## Running application

### Install dependencies

```
$ npm install
```

### Start

```
$ npm start
```